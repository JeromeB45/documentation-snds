# Glossaire
<!-- SPDX-License-Identifier: MPL-2.0 -->


## CNAM

Caisse Nationale d'Assurance Maladie

## DP

Diagnostic Principal

## RUM

Résumé d'Unité Médicale

## SLM

Section Locale Mutualiste